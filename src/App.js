import React,{useEffect, useState} from 'react'
import { StylesProvider, createGenerateClassName } from '@material-ui/core/styles'
import axios from 'axios'
import NotadinasBaruAll from './components/NotadinasBaruAll'
const generateClassName = createGenerateClassName({
    productionPrefix: 'suratbaru-notadinas',
  });
export default function App(props){
    return(
        <StylesProvider generateClassName={generateClassName}>
            <NotadinasBaruAll/>
         </StylesProvider>
    )
}